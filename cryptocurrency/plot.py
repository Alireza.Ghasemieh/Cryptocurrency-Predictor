import matplotlib.pyplot as plt
import pandas as pd


class Plot:
    @staticmethod
    def LTC_next_day():
        real_price = pd.read_csv('data/predicted/LTC_real_price.csv')
        predicted_price = pd.read_csv('data/predicted/LTC_proposed_predicted.csv')
        LSTM_price = pd.read_csv('data/predicted/LTC_LSTM_predicted.csv')

        plt.plot(real_price.iloc[:, 1:2].values, 'o-', color='red', label='Real Price')
        plt.plot(predicted_price, '.-', color='blue', label='Proposed Price')
        plt.plot(LSTM_price, '+-', color='green', label='LSTM Price')

        plt.title(f'LTC Next-Day Price Prediction')
        plt.xlabel('Time')
        plt.ylabel('Price')
        plt.legend()
        plt.savefig('result_plot/LTC_7day.png')
        plt.show()

    @staticmethod
    def LTC_7day():
        real_price = pd.read_csv('data/predicted/XMR_real_price.csv')
        _7_day_predicted_price = pd.read_csv('data/predicted/LTC_proposed_7day_predicted.csv')
        _7_day_LSTM_price = pd.read_csv('data/predicted/LTC_LSTM_7day_predicted.csv')

        plt.plot(real_price.iloc[:, 1:2].values, 'o-', color='red', label='Real Price')
        plt.plot(_7_day_predicted_price, '*-', color='blue', label='Proposed Price')
        plt.plot(_7_day_LSTM_price, '+-', color='green', label='LSTM Price')

        plt.title(f'LTC 7-Days Price Prediction')
        plt.xlabel('Time')
        plt.ylabel('Price')
        plt.legend()
        plt.savefig('result_plot/LTC_7day.png')
        plt.show()

    @staticmethod
    def XMR_next_day():
        real_price = pd.read_csv('data/predicted/XMR_real_price.csv')
        predicted_price = pd.read_csv('data/predicted/XMR_proposed_predicted.csv')
        LSTM_price = pd.read_csv('data/predicted/XMR_LSTM_predicted.csv')

        plt.plot(real_price.iloc[:, 1:2].values, 'o-', color='red', label='Real Price')
        plt.plot(predicted_price, '.-', color='blue', label='Proposed Price')
        plt.plot(LSTM_price, '+-', color='green', label='LSTM Price')

        plt.title(f'XMR Next-Day Price Prediction')
        plt.xlabel('Time')
        plt.ylabel('Price')
        plt.legend()
        plt.savefig('result_plot/XMR_next_day.png')
        plt.show()

    @staticmethod
    def XMR_7day():
        real_price = pd.read_csv('data/predicted/XMR_real_price.csv')
        _7_day_predicted_price = pd.read_csv('data/predicted/XMR_proposed_7day_predicted.csv')
        _7_day_LSTM_price = pd.read_csv('data/predicted/XMR_LSTM_7day_predicted.csv')

        plt.plot(real_price.iloc[:, 1:2].values, 'o-', color='red', label='Real Price')
        plt.plot(_7_day_predicted_price, '*-', color='blue', label='Proposed Price')
        plt.plot(_7_day_LSTM_price, '+-', color='green', label='LSTM Price')

        plt.title(f'XMR 7-Days Price Prediction')
        plt.xlabel('Time')
        plt.ylabel('Price')
        plt.legend()
        plt.savefig('result_plot/XMR_7day.png')
        plt.show()
