# Deep Learning Course Project

`Copyright Alireza Ghasemieh`

This project is a prediction engine, including the data ingestion pipeline and model selection process.

## Setup

We will use Python 3. Confirm that your version of python by running python --version. Note that you might have both
python 2 and python 3 installed, and that python 3 might be aliased to ```python3```.

If using Windows, ensure that Microsoft Visual Studio C++ build tools are installed https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019

We will be using a python Virtual Environment to manage project dependency versions.
For information on setting up a Python Virtual Environment, see https://sourabhbajaj.com/mac-setup/Python/virtualenv.html

Create a folder for a new vitual environment where dependencies will be stored. 

It is recommended to call it '<project-root>/.env' (.gitignore is already configured to ignore this folder)

```
$ python -m venv <project-root>/.env
```

Activate the environment (only necessary if not using Pycharm)
* Windows
    ```
    $ <project-root>/.env/Scripts/activate
    ```
* OSX & Linux    
    ```
    $ source <project-root>/.env/bin/activate
    ```
* From the project root, install the project dependencies 
    ```
    $ pip install -r requirements.txt
    ```
    If you get an SSL error, enjoy the life-changing magic of reading [this confluence page](https://confluence.i-proving.com/display/TOKB/Configuring+your+workstation+for+SSL+Content+Inspection)
* Add the deep learning module to the python site-packages for this venv by running the following:
    ```
    pwd > .env/Lib/site-packages/deep_learning.pth
    ```

If you are using PyCharm, be sure to use the Virtual Environment as the Project Interpreter.
This will allow PyCharm to load the necessary packages, as well as control package dependencies for you.

Windows:
* File > Settings > Project > Project Interpreter

OSX & Linux:
* PyCharm > Preferences > Project Interpreter 

See https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html

You must set up configurations for pipeline.py and sample_data_generator.py to execute properly:
* PyCharm > Run > Edit Configurations >
  
    * Edit the working directory field so that it points to the root of the project 

    * pipeline.py specific configurations (parameters)
        * `-D`
            * Denotes that source data should be downloaded from providers
        * `-T`
            * Denotes to perform data preprocessed
        * `-M`
            * Denotes to train the proposed models
        * `-L`
            * Denotes to train the LSTM baseline models
        * `-V`
            * Denotes the predictions and model performance evaluation are to be made
        * `-P`
            * Denotes to plot the result

``

## Running the Pipeline
To run the model generation pipeline, execute the `pipeline.py` file in the 
directory.  All output of the pipeline is written to the locations specified in config:
```buildoutcfg
[General]
data directory = data
data download directory = data/downloaded
data transform directory = data/transformed
model plot directory = model_plot
trained model directory = trained_model
predicted data directory = data/predicted
```

#### Command Line
For the first time please select all switches, but you can ignore `-M` and `-L` to avoid training the models and use the save ones.

Please always use `-T`.
```buildoutcfg
pipeline.py [-D] [-T] [-M] [-L] [-V] [-P]
```
#### PyCharm
You can create a run configuration in PyCharm for executing the pipeline.

## Pipeline Description
The main part of the pipeline is the `cryptocurrency` folder. It contains all essential part of the pipeline. 

* The first module is downloader which download the data from Binance API.

* The `transformer` is responsible to reformat the data as discussed in the project report.

* The `model_builder` is responsible to train both baseline and the proposed models.

* The `model_validation` is responsible to evaluate the performance of the models.

* The `plot` is responsible to generate plots.