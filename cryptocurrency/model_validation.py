from configuration import ConfigParser
from log_setup import get_logger
from pathlib import Path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
from sklearn.metrics import mean_squared_error, mean_absolute_error
from tensorflow.keras import models
from utils import save_csv
from pandas import DataFrame

logger = get_logger(__name__)


class Model_Validation:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.day_step = int(config['Preprocessing']['day_step'])
        self.trained_model_folder_path = Path(config['General']['trained model directory'])
        self.predicted_data_folder_path = Path(config['General']['predicted data directory'])

    def validate(self, model_name, train_set, test_set, sc):
        currency = model_name[:3]
        trained_model = models.load_model(f"{self.trained_model_folder_path}/{model_name}")
        training_set, testing_set = train_set.copy(), test_set.copy()
        real_price = testing_set.iloc[:, 1:2].values
        X_test = self.data_formating(train_set=training_set, test_set=testing_set, sc=sc)
        # Save the predicted data
        logger.info(f"Real price saved in {self.predicted_data_folder_path}/{currency}_real_price.csv")
        save_csv(testing_set, path=f"{self.predicted_data_folder_path}/{currency}_real_price.csv")

        # self.visualization(real_price=real_price, predicted_price=predicted_price, model_name=model_name)

        logger.info("----------Evaluation for 1-days prediction----------")
        result_1 = self.predict(trained_model, X_test, real_price, sc, number_day=1)
        self.evaluation_metric(real_price=real_price, predicted_price=result_1)
        logger.info(f"Predicted price saved in {self.predicted_data_folder_path}/{model_name}_1_day.csv")
        save_csv(DataFrame(result_1), path=f"{self.predicted_data_folder_path}/{model_name}_1_day.csv")
        logger.info("----------Evaluation for 3-days prediction----------")
        result_3 = self.predict(trained_model, X_test, real_price, sc, number_day=3)
        result_3 = np.array([result_3[0], result_3[1], result_3[2], result_3[5], result_3[8], result_3[11], result_3[14]])
        self.evaluation_metric(real_price=real_price, predicted_price=result_3)
        logger.info(f"Predicted price saved in {self.predicted_data_folder_path}/{model_name}_3_day.csv")
        save_csv(DataFrame(result_3), path=f"{self.predicted_data_folder_path}/{model_name}_3_day.csv")
        logger.info("----------Evaluation for 7-days prediction----------")
        result_7 = self.predict(trained_model, X_test, real_price, sc, number_day=7)
        self.evaluation_metric(real_price=real_price, predicted_price=result_7)
        logger.info(f"Predicted price saved in {self.predicted_data_folder_path}/{model_name}_7_day.csv")
        save_csv(DataFrame(result_7), path=f"{self.predicted_data_folder_path}/{model_name}_7_day.csv")


    @staticmethod
    def predict(trained_model, X_test, y_test, sc, number_day=1):
        result = []
        for i in range(X_test.shape[0] - number_day + 1):
            new_train = np.reshape(X_test[i], (1, X_test[0].shape[0], X_test[0].shape[1]))
            for j in range(number_day):
                next_day = trained_model.predict(new_train)
                new_train = np.reshape(np.concatenate((new_train[0], next_day))[-30:],
                                       (1, X_test[0].shape[0], X_test[0].shape[1]))
                predicted_price = sc.inverse_transform(next_day)
                result.append(predicted_price[0][0])
        return np.flipud(result)

    def data_formating(self, train_set, test_set, sc):
        logger.info(f"Creating a data structure with {self.day_step} time steps")
        # Getting the predicted price
        dataset_total = pd.concat((train_set['Mean_Price'], test_set['Mean_Price']), axis=0)
        inputs = dataset_total[len(dataset_total) - len(test_set) - self.day_step:].values
        # Reshape
        inputs = inputs.reshape(-1, 1)
        # Scale the input
        inputs = sc.transform(inputs)
        # Creating a data structure with 30 time steps and 1 output
        X_test = []
        for i in range(self.day_step, self.day_step + len(test_set)):
            X_test.append(inputs[i - self.day_step:i, 0])
        X_test = np.array(X_test)
        # Reshaping
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
        return X_test

    @staticmethod
    def visualization(real_price, predicted_price, model_name):
        plt.plot(real_price, 'o-', color='red', label='Real Price')
        plt.plot(predicted_price, '.-', color='blue', label='Proposed Predicted Price')
        plt.title(f'{model_name} Price Prediction')
        plt.xlabel('Time')
        plt.ylabel('Price')
        plt.legend()
        plt.show()

    def evaluation_metric(self, real_price, predicted_price):
        mse = mean_squared_error(real_price, predicted_price)
        logger.info(f"The Mean Square Error (MSE) is {mse}")

        rmse = math.sqrt(mean_squared_error(real_price, predicted_price))
        logger.info(f"The Root Mean Square Error (RMSE) is {rmse}")

        mae = mean_absolute_error(real_price, predicted_price)
        logger.info(f"The Mean Absolute Error (MAE) is {mae}")

        mspe = self.mean_absolute_percentage_error(real_price, predicted_price)
        logger.info(f"The Mean Absolute Percentage Error (MAPE) is {mspe}")

    @staticmethod
    def mean_absolute_percentage_error(y_true, y_pred):
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100
