import configuration
import yfinance as yf
from pathlib import Path
from utils import save_csv
from pandas import DataFrame
from datetime import datetime
from log_setup import get_logger
from configuration import ConfigParser

logger = get_logger(__name__)


class Downloader:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.downloaded_folder_path = Path(config['General']['data download directory'])

    def download(self, symbol, start, end, interval):
        data = yf.Ticker(symbol)
        # get historical market data
        hist = data.history(interval=interval, start=start, end=end)
        hist.reset_index(level=0, inplace=True)
        hist['Date'] = hist['Date'].map(lambda x: datetime.date(x))

        logger.info(f"Data shape for {symbol} is {hist.shape}")
        save_csv(hist, Path(f"{self.downloaded_folder_path}/{symbol}_{start}_{end}.csv"))


if __name__ == '__main__':
    symbol = "XMR-USD"
    start = "2020-01-30"
    end = "2020-02-23"
    interval = "1d"
    download: Downloader = Downloader(configuration.get())
    download.download(symbol, start=start, end=end, interval=interval)
