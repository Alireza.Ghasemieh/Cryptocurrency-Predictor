import sys
import argparse
import configuration
from typing import List
from pathlib import Path
from pandas import read_csv
from argparse import Namespace
from log_setup import get_logger
from cryptocurrency.plot import Plot
from utils import create_directory
from argparse import ArgumentParser
from configuration import ConfigParser
from pipeline_report import PipelineReport
from cryptocurrency.downloader import Downloader
from cryptocurrency.transformation import Transformation
from cryptocurrency.model_builder import Model
from cryptocurrency.model_validation import Model_Validation

logger = get_logger(__name__)


class Pipeline:
    def __init__(self, config: ConfigParser):
        self.pipeline_report = PipelineReport()
        self.config = config
        self.data_folder_path = Path(config['General']['data directory'])
        self.downloaded_folder_path = Path(config['General']['data download directory'])
        self.model_plot_folder_path = Path(config['General']['model plot directory'])
        self.trained_model_folder_path = Path(config['General']['trained model directory'])
        self.predicted_data_folder_path = Path(config['General']['predicted data directory'])
        self.predicted_data_folder_path = Path(config['General']['predicted data directory'])
        self.epoch = int(config['Training']['epoch'])
        self.epoch_LSTM = int(config['Training']['LSTM epoch'])

    def pipeline(self, arguments: List[str]) -> None:
        logger.info("+----------------------------------+")
        logger.info("| Pipeline started. |")
        logger.info("+----------------------------------+")

        self.pipeline_report.arguments = ' '.join(arguments)

        parser: ArgumentParser = argparse.ArgumentParser(description=__doc__)

        parser.add_argument("-D", "--download", help="download raw data files", action="store_true")
        parser.add_argument("-T", "--transformation", help="download raw data files", action="store_true")
        parser.add_argument("-M", "--proposed_model", help="download raw data files", action="store_true")
        parser.add_argument("-L", "--LSTM", help="download raw data files", action="store_true")
        parser.add_argument("-V", "--validation", help="download raw data files", action="store_true")
        parser.add_argument("-P", "--plot", help="download raw data files", action="store_true")

        args: Namespace = parser.parse_args(args=arguments)

        create_directory(self.data_folder_path)
        create_directory(self.downloaded_folder_path)
        create_directory(self.model_plot_folder_path)
        create_directory(self.trained_model_folder_path)
        create_directory(self.predicted_data_folder_path)

        try:
            if args.download:
                logger.info("************------------( Data downloader started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Data Downloader")
                try:
                    logger.info("Litecoin data downloading")
                    LTC_symbol = "LTC-USD"
                    LTC_start = "2016-08-24"
                    LTC_end = "2020-02-23"
                    LTC_interval = "1d"
                    downloader = Downloader(configuration.get())
                    downloader.download(LTC_symbol, start=LTC_start, end=LTC_end, interval=LTC_interval)

                    logger.info("Monero data downloading")
                    XMR_symbol = "XMR-USD"
                    XMR_start = "2015-01-30"
                    XMR_end = "2020-02-23"
                    XMR_interval = "1d"
                    downloader = Downloader(configuration.get())
                    downloader.download(XMR_symbol, start=XMR_start, end=XMR_end, interval=XMR_interval)

                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.transformation:
                logger.info("************------------( Transformation started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Transformation")
                try:
                    transformer = Transformation(configuration.get())
                    logger.info("Litecoin data transformation")
                    LTC_path = Path(f"{self.downloaded_folder_path}/{LTC_symbol}_{LTC_start}_{LTC_end}.csv")
                    LTC_USD = read_csv(LTC_path)
                    LTC_X_train, LTC_y_train, LTC_train_set, LTC_test_set, LTC_sc = transformer.transform(data=LTC_USD)

                    logger.info("Monero data transformation")
                    XMR_path = Path(f"{self.downloaded_folder_path}/{XMR_symbol}_{XMR_start}_{XMR_end}.csv")
                    XMR_USD = read_csv(XMR_path)
                    XMR_X_train, XMR_y_train, XMR_train_set, XMR_test_set, XMR_sc = transformer.transform(data=XMR_USD)

                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.proposed_model:
                logger.info("************------------( Proposed Model training started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Proposed Model Building")
                try:
                    proposed_model = Model(configuration.get())
                    logger.info("Litecoin proposed model training")
                    proposed_model.proposed_model_build(model_name="LTC_proposed",
                                                        X_train=LTC_X_train,
                                                        y_train=LTC_y_train,
                                                        epoch=self.epoch)

                    logger.info("Monero proposed model training")
                    proposed_model.proposed_model_build(model_name="XMR_proposed",
                                                        X_train=XMR_X_train,
                                                        y_train=XMR_y_train,
                                                        epoch=self.epoch)
                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.LSTM:
                logger.info("************------------( LSTM training started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("LSTM Model Building")
                try:
                    LSTM_model = Model(configuration.get())
                    logger.info("Litecoin LSTM training")
                    LSTM_model.LSTM_model_build(model_name="LTC_LSTM",
                                                X_train=LTC_X_train,
                                                y_train=LTC_y_train,
                                                epoch=self.epoch_LSTM)

                    logger.info("Monero LSTM training")
                    LSTM_model.LSTM_model_build(model_name="XMR_LSTM",
                                                X_train=XMR_X_train,
                                                y_train=XMR_y_train,
                                                epoch=self.epoch_LSTM)

                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.validation:
                logger.info("************------------( Model Validation started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Model Validation")
                try:
                    validation = Model_Validation(configuration.get())
                    # if args.proposed_model:
                    logger.info("Litecoin Proposed Model Validation")
                    validation.validate(model_name="LTC_proposed", train_set=LTC_train_set,
                                        test_set=LTC_test_set, sc=LTC_sc)
                    logger.info("Monero Proposed Model Validation")
                    validation.validate(model_name="XMR_proposed", train_set=XMR_train_set,
                                        test_set=XMR_test_set, sc=XMR_sc)
                    # if args.LSTM:
                    logger.info("Litecoin LSTM Model Validation")
                    validation.validate(model_name="LTC_LSTM", train_set=LTC_train_set,
                                        test_set=LTC_test_set, sc=LTC_sc)
                    logger.info("Monero LSTM Model Validation")
                    validation.validate(model_name="XMR_LSTM", train_set=XMR_train_set,
                                        test_set=XMR_test_set, sc=XMR_sc)

                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            if args.plot:
                logger.info("************------------( Model Validation started )------------************")
                pipeline_report_step = self.pipeline_report.create_step("Model Validation")
                try:
                    Plot.LTC_7day()
                    Plot.LTC_next_day()
                    Plot.XMR_7day()
                    Plot.XMR_next_day()

                except BaseException as e:
                    pipeline_report_step.mark_failure(str(e))
                    raise e

            self.pipeline_report.mark_success()
        except BaseException as e:
            self.pipeline_report.mark_failure()
            raise e

        finally:
            self.pipeline_report.log()


if __name__ == '__main__':
    pipeline: Pipeline = Pipeline(configuration.get())
    pipeline.pipeline(sys.argv[1:])
