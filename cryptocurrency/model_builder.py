from pathlib import Path
import tensorflow as tf
from tensorflow.keras import Input, Sequential
from tensorflow.keras.layers import Dropout, Flatten, Dense
from tensorflow.keras.layers import LSTM, GRU
from tensorflow.keras.utils import plot_model
from configuration import ConfigParser
from log_setup import get_logger

logger = get_logger(__name__)


class Model:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.model_plot_folder_path = Path(config['General']['model plot directory'])
        self.trained_model_folder_path = Path(config['General']['trained model directory'])

    def proposed_model_build(self, model_name, X_train, y_train, epoch=100):
        # Initialization the RNN
        input_shape = Input(shape=(X_train.shape[1], 1))
        # Adding the first LSTM layer and some Dropout regularization
        LSTM_regressor = LSTM(units=30, return_sequences=True, activation='relu')(input_shape)
        LSTM_regressor = Dropout(0.2)(LSTM_regressor)
        # Adding the second LSTM layer and some Dropout regularization
        LSTM_regressor = LSTM(units=50, return_sequences=False, activation='relu')(LSTM_regressor)
        # regressor.add(Dropout(0.2))
        # Adding output layer
        LSTM_regressor = Dense(units=1)(LSTM_regressor)
        # Adding the GRU layer and some Dropout regularization
        GRU_regressor = GRU(units=30, return_sequences=False, activation='relu')(input_shape)
        GRU_regressor = Dropout(0.2)(GRU_regressor)
        # Adding output layer
        GRU_regressor = Dense(units=1)(GRU_regressor)
        merged = tf.keras.layers.concatenate([LSTM_regressor, GRU_regressor], axis=1)
        # merged = Flatten()(merged)
        out = Dense(units=1)(merged)
        model = tf.keras.Model(input_shape, out)
        logger.info(f"Model scheme saved in {self.model_plot_folder_path}/{model_name}.png")
        plot_model(model, to_file=f'{self.model_plot_folder_path}/{model_name}.png', show_shapes=True)
        # Compiling the RNN
        model.compile(optimizer='Adam', loss='mean_squared_error')
        # Fitting the RNN to training set
        logger.info(f"Start training the model {model_name}")
        model.fit(X_train, y_train, batch_size=32, epochs=epoch)
        # Save the model
        logger.info(f"Model saved in {self.trained_model_folder_path}/{model_name}")
        model.save(filepath=f"{self.trained_model_folder_path}/{model_name}")

    def LSTM_model_build(self, model_name, X_train, y_train, epoch=100):
        # Initialization the RNN
        model = Sequential()
        # Adding the first LSTM layer and some Dropout regularization
        model.add(LSTM(units=50, return_sequences=False, input_shape=(X_train.shape[1], 1), activation='relu'))
        # Adding output layer
        model.add(Dense(units=1))
        logger.info(f"Model scheme saved in {self.model_plot_folder_path}/{model_name}.png")
        plot_model(model, to_file=f'{self.model_plot_folder_path}/{model_name}.png', show_shapes=True)
        # Compiling the RNN
        model.compile(optimizer='Adam', loss='mean_squared_error')
        # Fitting the RNN to training set
        logger.info(f"Start training the model {model_name}")
        model.fit(X_train, y_train, batch_size=32, epochs=epoch)
        # Save the model
        logger.info(f"Model saved in {self.trained_model_folder_path}/{model_name}")
        model.save(filepath=f"{self.trained_model_folder_path}/{model_name}")


