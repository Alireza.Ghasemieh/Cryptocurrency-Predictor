import numpy as np
from pandas import DataFrame, read_csv
from sklearn.preprocessing import MinMaxScaler

import configuration
from configuration import ConfigParser
from log_setup import get_logger

logger = get_logger(__name__)


class Transformation:
    def __init__(self, config: ConfigParser):
        self.config = config
        self.splitting_percentage = float(config['Preprocessing']['splitting_percentage'])
        self.day_step = int(config['Preprocessing']['day_step'])

    def transform(self, data):
        data_df = data.copy()
        data_df.drop(columns=['Volume', 'Dividends', 'Stock Splits'], inplace=True)
        data_df = self.average_price(data=data_df)
        data_df.drop(columns=['Open', 'High', 'Low', 'Close'], inplace=True)

        train_set = data_df[:-7]
        test_set = data_df[-7:]
        del data_df

        sc = MinMaxScaler(feature_range=(0, 1))
        train_scaled = sc.fit_transform(train_set.iloc[:, 1:2].values)
        logger.info(f"Training dataset is normalized")

        X_train, y_train = self.data_formating(data=train_scaled, day_step=self.day_step)
        del train_scaled

        # Reshaping
        X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

        return X_train, y_train, train_set, test_set, sc

    @staticmethod
    def average_price(data) -> DataFrame:
        logger.info("Taking average of Open, Close, Low, and High price")
        data['Mean_Price'] = data[['Open', 'High', 'Low', 'Close']].apply(lambda x: x.mean(), axis=1)
        return data

    @staticmethod
    def data_formating(data, day_step=30):
        logger.info(f"Creating a data structure with {day_step} time steps and 1 output")
        # Creating a data structure with 30 time steps and 1 output
        X_train = []
        y_train = []
        for i in range(day_step, len(data)):
            X_train.append(data[i - day_step:i, 0])
            y_train.append(data[i, 0])
        return np.array(X_train), np.array(y_train)


if __name__ == '__main__':
    data_df = read_csv("data/downloaded/LTC-USD_2016-08-24_2020-02-23.csv")
    transform: Transformation = Transformation(configuration.get())
    transform.transform(data=data_df)
